.PHONY: all
all: shaders rust

.PHONY: rust
rust: $(wildcard src/*)
	cargo build

.PHONY: shaders
shaders: shaders/vert.spv shaders/frag.spv

%.spv: shader.%
	glslc $< -o $(dir $<)$(subst .,,$(suffix $<)).spv
