use crate::prelude::*;

#[derive(Debug, Error)]
#[error("Missing {0}.")]
pub(crate) struct SuitabilityError(pub(crate) &'static str);
