use crate::prelude::*;

pub(crate) unsafe fn create(
    instance: &Instance,
    device: &Device,
    data: &mut AppData,
) -> Result<()> {
    let size = (size_of::<u16>() * INDICES.len()) as u64;

    let (staging_buffer, staging_buffer_memory) = buffer::create(
        instance,
        device,
        data,
        size,
        vk::BufferUsageFlags::TRANSFER_SRC,
        vk::MemoryPropertyFlags::HOST_COHERENT | vk::MemoryPropertyFlags::HOST_VISIBLE,
    )?;

    let memory = device.map_memory(staging_buffer_memory, 0, size, vk::MemoryMapFlags::empty())?;

    memcopy(INDICES.as_ptr(), memory.cast(), INDICES.len());
    device.unmap_memory(staging_buffer_memory);

    let (index_buffer, index_buffer_memory) = buffer::create(
        instance,
        device,
        data,
        size,
        vk::BufferUsageFlags::TRANSFER_DST | vk::BufferUsageFlags::INDEX_BUFFER,
        vk::MemoryPropertyFlags::DEVICE_LOCAL,
    )?;

    data.index_buffer = index_buffer;
    data.index_buffer_memory = index_buffer_memory;

    buffer::copy(device, data, staging_buffer, data.index_buffer, size)?;

    device.free_memory(staging_buffer_memory, None);
    device.destroy_buffer(staging_buffer, None);

    Ok(())
}

pub(crate) unsafe fn destroy(device: &Device, data: &mut AppData) {
    buffer::destroy(device, data.index_buffer, data.index_buffer_memory);
}
