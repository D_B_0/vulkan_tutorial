#![allow(
    dead_code,
    unused_variables,
    clippy::too_many_arguments,
    clippy::unnecessary_wraps
)]

mod app;
mod constants;
mod prelude;
mod vertex;
mod vulkan_functions;

use prelude::*;

fn main() -> Result<()> {
    pretty_env_logger::init();

    let event_loop = EventLoop::new();
    let window = WindowBuilder::new()
        .with_title("Vulkan tutorial")
        .with_inner_size(LogicalSize::new(1024, 768))
        .build(&event_loop)?;

    let mut app = unsafe { App::create(&window)? };
    let mut minimized = false;
    let mut destroying = false;
    event_loop.run(move |event, _event_loop, control_flow| {
        *control_flow = ControlFlow::Poll;
        match event {
            Event::MainEventsCleared if !destroying && !minimized => {
                unsafe { app.render(&window) }.unwrap();
            }
            Event::WindowEvent {
                event: WindowEvent::CloseRequested,
                ..
            } => {
                destroying = true;
                *control_flow = ControlFlow::Exit;
                unsafe { app.destroy() };
            }
            Event::WindowEvent {
                event: WindowEvent::Resized(new_size),
                ..
            } => {
                if new_size.width == 0 && new_size.height == 0 {
                    minimized = true;
                } else {
                    debug!("Window resized: {:?}", new_size);
                    minimized = false;
                    app.resized = true;
                }
            }
            _ => {}
        }
    });
}

mod vk_debug {
    use log::*;
    use std::{ffi::CStr, os::raw::c_void};
    use vulkanalia::vk::{
        Bool32, DebugUtilsMessageSeverityFlagsEXT, DebugUtilsMessageTypeFlagsEXT,
        DebugUtilsMessengerCallbackDataEXT, FALSE,
    };

    pub(crate) extern "system" fn callback(
        severity: DebugUtilsMessageSeverityFlagsEXT,
        type_: DebugUtilsMessageTypeFlagsEXT,
        data: *const DebugUtilsMessengerCallbackDataEXT,
        _: *mut c_void,
    ) -> Bool32 {
        let data = unsafe { *data };
        let message = unsafe { CStr::from_ptr(data.message) }.to_string_lossy();

        if severity >= DebugUtilsMessageSeverityFlagsEXT::ERROR {
            error!("({type_:?}) {message}");
        } else if severity >= DebugUtilsMessageSeverityFlagsEXT::WARNING {
            warn!("({type_:?}) {message}");
        } else if severity >= DebugUtilsMessageSeverityFlagsEXT::INFO {
            debug!("({type_:?}) {message}");
        } else {
            trace!("({type_:?}) {message}");
        }
        FALSE
    }
}
