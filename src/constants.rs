use crate::prelude::*;

pub(crate) const VALIDATION_ENABLED: bool = cfg!(debug_assertions);
pub(crate) const VALIDATION_LAYER: vk::ExtensionName =
    vk::ExtensionName::from_bytes(b"VK_LAYER_KHRONOS_validation");
pub(crate) const DEVICE_EXTENSIONS: &[vk::ExtensionName] = &[vk::KHR_SWAPCHAIN_EXTENSION.name];
pub(crate) const MAX_FRAMES_IN_FLIGHT: usize = 2;

lazy_static! {
    pub(crate) static ref VERTICES: Vec<Vertex> = vec![
        Vertex::new(glm::Vec2::new(-0.5, -0.5), glm::Vec3::new(1.0, 0.0, 0.0)),
        Vertex::new(glm::Vec2::new(0.5, -0.5), glm::Vec3::new(0.0, 1.0, 0.0)),
        Vertex::new(glm::Vec2::new(0.5, 0.5), glm::Vec3::new(0.0, 0.0, 1.0)),
        Vertex::new(glm::Vec2::new(-0.5, 0.5), glm::Vec3::new(1.0, 1.0, 1.0)),
    ];
}
pub(crate) const INDICES: &[u16] = &[0, 1, 2, 2, 3, 0];
