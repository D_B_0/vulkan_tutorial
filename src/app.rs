use crate::prelude::*;

#[derive(Clone, Debug)]
pub(crate) struct App {
    pub(crate) entry: Entry,
    pub(crate) instance: Instance,
    pub(crate) data: AppData,
    pub(crate) device: Device,
    pub(crate) frame: usize,
    pub(crate) resized: bool,
}

impl App {
    pub(crate) unsafe fn create(window: &Window) -> Result<Self> {
        let loader = LibloadingLoader::new(LIBRARY)?;
        let entry = Entry::new(loader).map_err(|b| anyhow!("{}", b))?;
        let mut data = AppData::default();

        // create instance & debug messenger in this order
        let instance = instance::create(window, &entry, &mut data)?;
        // create surface
        data.surface = vk_window::create_surface(&instance, window)?;
        // create physical device
        device::pick_physical(&instance, &mut data)?;
        let device = device::create_logical(&instance, &mut data)?;
        // create swapchain & image views
        swapchain::create(window, &instance, &device, &mut data)?;
        // create rendeer pass
        render_pass::create(&instance, &device, &mut data)?;
        // create pipeline layout & pipeline in this order
        pipeline::create(&device, &mut data)?;
        // create framebuffers
        pipeline::create_framebuffers(&device, &mut data)?;
        // create command pool
        commands::create_pool(&instance, &device, &mut data)?;
        // create vertex buffer
        vertex_buffer::create(&instance, &device, &mut data)?;
        // create index buffer
        index_buffer::create(&instance, &device, &mut data)?;

        commands::create_buffers(&device, &mut data)?;
        // create sync objects
        sync_objects::create(&device, &mut data)?;
        Ok(Self {
            entry,
            instance,
            data,
            device,
            frame: 0,
            resized: false,
        })
    }

    pub(crate) unsafe fn render(&mut self, window: &Window) -> Result<()> {
        let frame = self.frame % MAX_FRAMES_IN_FLIGHT;

        self.device
            .wait_for_fences(&[self.data.in_flight_fences[frame]], true, u64::MAX)?;

        let result = self.device.acquire_next_image_khr(
            self.data.swapchain,
            u64::MAX,
            self.data.image_available_semaphores[frame],
            vk::Fence::null(),
        );
        let image_index = match result {
            Ok((image_index, _)) => image_index as usize,
            Err(vk::ErrorCode::OUT_OF_DATE_KHR) => return self.recreate_swapchain(window),
            Err(e) => return Err(anyhow!(e)),
        };

        if !self.data.images_in_flight[image_index].is_null() {
            self.device.wait_for_fences(
                &[self.data.images_in_flight[image_index]],
                true,
                u64::MAX,
            )?;
        }

        self.data.images_in_flight[image_index] = self.data.in_flight_fences[frame];

        let wait_semaphores = &[self.data.image_available_semaphores[frame]];
        let wait_stages = &[vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT];
        let command_buffers = &[self.data.command_buffers[image_index]];
        let signal_semaphores = &[self.data.render_finished_semaphores[frame]];
        let submit_info = vk::SubmitInfo::builder()
            .wait_semaphores(wait_semaphores)
            .wait_dst_stage_mask(wait_stages)
            .command_buffers(command_buffers)
            .signal_semaphores(signal_semaphores);

        self.device
            .reset_fences(&[self.data.in_flight_fences[frame]])?;

        self.device.queue_submit(
            self.data.graphics_queue,
            &[submit_info],
            self.data.in_flight_fences[frame],
        )?;

        let swapchains = &[self.data.swapchain];
        let image_indices = &[image_index as u32];
        let present_info = vk::PresentInfoKHR::builder()
            .wait_semaphores(signal_semaphores)
            .swapchains(swapchains)
            .image_indices(image_indices);

        let result = self
            .device
            .queue_present_khr(self.data.present_queue, &present_info);

        let changed = result == Ok(vk::SuccessCode::SUBOPTIMAL_KHR)
            || result == Err(vk::ErrorCode::OUT_OF_DATE_KHR);

        if changed || self.resized {
            self.recreate_swapchain(window)?;
        } else if let Err(e) = result {
            return Err(anyhow!(e));
        }

        self.resized = false;

        if self.frame == usize::MAX {
            self.frame = 0;
        } else {
            self.frame += 1;
        }
        Ok(())
    }

    unsafe fn recreate_swapchain(&mut self, window: &Window) -> Result<()> {
        self.device.device_wait_idle()?;
        debug!("Recreating swapchain");
        self.destroy_swapchain();
        // recreate swapchain & image views
        swapchain::create(window, &self.instance, &self.device, &mut self.data)?;
        // recreate rendeer pass
        render_pass::create(&self.instance, &self.device, &mut self.data)?;
        // recreate pipeline layout & pipeline
        pipeline::create(&self.device, &mut self.data)?;
        // recreate framebuffers
        pipeline::create_framebuffers(&self.device, &mut self.data)?;
        // recreate command buffers
        commands::create_buffers(&self.device, &mut self.data)?;

        self.data
            .images_in_flight
            .resize(self.data.swapchain_images.len(), vk::Fence::null());

        Ok(())
    }

    unsafe fn destroy_swapchain(&mut self) {
        // destroy framebuffers
        pipeline::destroy_framebuffers(&self.device, &mut self.data);
        // free command buffers for possible reuse of the command pool
        commands::free_buffers(&self.device, &mut self.data);
        // destroy pipeline & pipeline layout
        pipeline::destroy(&self.device, &mut self.data);
        // destroy render pass
        render_pass::destroy(&self.device, &mut self.data);
        // destroy swapchain image views & swapchain
        swapchain::destroy(&self.device, &mut self.data);
    }

    pub(crate) unsafe fn destroy(&mut self) {
        self.device.device_wait_idle().unwrap();
        // destroy sync objects
        sync_objects::destroy(&self.device, &mut self.data);
        // destroy anything related to the swapchain
        self.destroy_swapchain();
        // destroy index buffer
        index_buffer::destroy(&self.device, &mut self.data);
        // destroy vertex buffer
        vertex_buffer::destroy(&self.device, &mut self.data);
        // destroy command pool
        commands::destroy_pool(&self.device, &mut self.data);
        // destroy physical device
        self.device.destroy_device(None);
        // destroy surface
        self.instance.destroy_surface_khr(self.data.surface, None);
        // destroy debug messenger
        if VALIDATION_ENABLED {
            self.instance
                .destroy_debug_utils_messenger_ext(self.data.messenger, None);
        }
        // destroy instance
        self.instance.destroy_instance(None);
    }
}

#[derive(Clone, Debug, Default)]
pub(crate) struct AppData {
    pub(crate) surface: vk::SurfaceKHR,
    pub(crate) messenger: vk::DebugUtilsMessengerEXT,
    pub(crate) physical_device: vk::PhysicalDevice,
    pub(crate) graphics_queue: vk::Queue,
    pub(crate) present_queue: vk::Queue,
    pub(crate) swapchain_format: vk::Format,
    pub(crate) swapchain_extent: vk::Extent2D,
    pub(crate) swapchain: vk::SwapchainKHR,
    pub(crate) swapchain_images: Vec<vk::Image>,
    pub(crate) swapchain_image_views: Vec<vk::ImageView>,
    pub(crate) render_pass: vk::RenderPass,
    pub(crate) pipeline_layout: vk::PipelineLayout,
    pub(crate) pipeline: vk::Pipeline,
    pub(crate) framebuffers: Vec<vk::Framebuffer>,
    pub(crate) command_pool: vk::CommandPool,
    pub(crate) vertex_buffer: vk::Buffer,
    pub(crate) vertex_buffer_memory: vk::DeviceMemory,
    pub(crate) index_buffer: vk::Buffer,
    pub(crate) index_buffer_memory: vk::DeviceMemory,
    pub(crate) command_buffers: Vec<vk::CommandBuffer>,
    pub(crate) image_available_semaphores: Vec<vk::Semaphore>,
    pub(crate) render_finished_semaphores: Vec<vk::Semaphore>,
    pub(crate) in_flight_fences: Vec<vk::Fence>,
    pub(crate) images_in_flight: Vec<vk::Fence>,
}
