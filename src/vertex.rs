use crate::prelude::*;

#[repr(C)]
#[derive(Debug, Clone, Copy)]
pub(crate) struct Vertex {
    pub(crate) pos: glm::Vec2,
    pub(crate) col: glm::Vec3,
}

impl Vertex {
    pub(crate) fn new(pos: glm::Vec2, col: glm::Vec3) -> Self {
        Self { pos, col }
    }

    pub(crate) fn binding_description() -> vk::VertexInputBindingDescription {
        vk::VertexInputBindingDescription::builder()
            .binding(0)
            .stride(size_of::<Vertex>() as u32)
            .input_rate(vk::VertexInputRate::VERTEX)
            .build()
    }

    pub(crate) fn attribute_description() -> [vk::VertexInputAttributeDescription; 2] {
        let pos = vk::VertexInputAttributeDescription::builder()
            .binding(0)
            .location(0)
            .format(vk::Format::R32G32_SFLOAT)
            .offset(0)
            .build();

        let col = vk::VertexInputAttributeDescription::builder()
            .binding(0)
            .location(1)
            .format(vk::Format::R32G32B32_SFLOAT)
            .offset(size_of::<glm::Vec2>() as u32)
            .build();

        [pos, col]
    }
}
